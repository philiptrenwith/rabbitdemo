package org.trenwith.rabbitmq;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.SerializationUtils;
/**
 * https://www.rabbitmq.com/api-guide.html
 * 
 * https://dzone.com/articles/getting-started-rabbitmq-java
 * 
 * @author Philip M. Trenwith
 */
public class RabbitMQConsumer extends EndPoint implements Runnable, Consumer{
	
	public RabbitMQConsumer(String endPointName) throws IOException{
		super(endPointName);		
	}
	
	public void run() 
        {
            try 
            {
                //start consuming messages. Auto acknowledge messages.
		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                    String message = SerializationUtils.deserialize(delivery.getBody()) + "";
                    System.out.println(" [x] Received '" + message + "'");
                };
            channel.basicConsume(endPointName, true, deliverCallback, consumerTag -> { });
            } catch (IOException e) {
		e.printStackTrace();
            }
	}

	/**
	 * Called when consumer is registered.
	 */
	public void handleConsumeOk(String consumerTag) {
		System.out.println("Consumer " + consumerTag + " registered");		
	}

	/**
	 * Called when new message is available.
	 */
	public void handleDelivery(String consumerTag, Envelope env,
			BasicProperties props, byte[] body) throws IOException 
        {
            Map map = (HashMap)SerializationUtils.deserialize(body);
	    System.out.println("Message Number "+ map.get("message number") + " received.");
	}

	public void handleCancel(String consumerTag) 
        {
            System.out.println("Consumer Tag cancelled: "+ consumerTag);
        }
        
	public void handleCancelOk(String consumerTag) 
        {
            System.out.println("Consumer Tag CancelOk: "+ consumerTag);
        }
        
	public void handleRecoverOk(String consumerTag) 
        {
            System.out.println("Consumer Tag RecoverOk: "+ consumerTag);
        }
        
	public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) 
        {
            System.out.println("Consumer Tag Shutdown Signal: "+ consumerTag);
        }
        
}