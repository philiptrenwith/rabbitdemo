package org.trenwith.main;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.trenwith.rabbitmq.RabbitMQConsumer;

/**
 *
 * @author Philip M. Trenwith
 */
public class ConsumerMainTester {
    
    private RabbitMQConsumer consumer;
    
    public static void main(String[] args)
    {
        ConsumerMainTester tester = new ConsumerMainTester();
        tester.run();
    }
    
    public void run()
    {
        try {
            consumer = new RabbitMQConsumer("lucene-queue");
            Thread consumerThread = new Thread(consumer);
            System.out.println("I am listening... ");
            consumerThread.start();
        } catch (IOException ex) {
            Logger.getLogger(ConsumerMainTester.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
