package org.trenwith.main;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.trenwith.rabbitmq.RabbitMQPublisher;

/**
 *
 * @author Philip M. Trenwith
 */
public class PublisherMainTester {
    
    private RabbitMQPublisher publisher;
    private SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY hh:mm:ss");
    private int publishInterval = 2000;
    
    public static void main(String[] args)
    {
        PublisherMainTester tester = new PublisherMainTester();
        tester.run();
    }
    
    public void run()
    {
        try {
            publisher = new RabbitMQPublisher("lucene-queue");
        } catch (IOException ex) {
            Logger.getLogger(PublisherMainTester.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int i = 0;
        while (true)
        {
            try {
                HashMap message = new HashMap();
                message.put("Hello " + sdf.format(new Date(System.currentTimeMillis())), i++);

                publisher.publish(message);
                Thread.sleep(publishInterval);
            } catch (InterruptedException ex) {
                Logger.getLogger(PublisherMainTester.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PublisherMainTester.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
