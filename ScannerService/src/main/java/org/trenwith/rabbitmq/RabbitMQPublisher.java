
package org.trenwith.rabbitmq;

import java.io.IOException;
import java.io.Serializable;
import org.apache.commons.lang.SerializationUtils;

/**
 *
 * @author Philip M. Trenwith
 */
public class RabbitMQPublisher extends EndPoint
{
	
	public RabbitMQPublisher(String endPointName) throws IOException {
		super(endPointName);
	}

	public void publish(Serializable object) throws IOException {
            System.out.println("Publishing: '" + object + "' on queue: " + endPointName);
	    channel.basicPublish("",endPointName, null, SerializationUtils.serialize(object));
	}	
}
